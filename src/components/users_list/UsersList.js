import User from "./user/User";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect } from "react";
import { getAll } from "../../store/actionCreators/userActionCreators";
import { Box } from '@mui/material';
import { makeStyles } from "@mui/styles";

const UsersList = () => {
    const dispatch = useDispatch();
    const users_list = useSelector((state) => state.user.users_list);

    const getUsers = useCallback(async () => {
        await dispatch(getAll());
    }, [dispatch])
    
    useEffect(() => {
        getUsers();
    }, [dispatch, getUsers])

    const useStyles = makeStyles({
        app__usersList: {
            width: "600px",
            margin: "0 auto"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__usersList}>
            {users_list.length > 0 && (
                users_list.map((user) => (
                    <User key={user.id} name={user.user_name} surname={user.user_surname} email={user.user_email} />
                ))
            )}
        </Box>
    )
}

export default UsersList;