import { Box, Typography } from '@mui/material';
import { makeStyles } from "@mui/styles";

const User = (props) => {
    const useStyles = makeStyles({
        app__usersList__user: {
            marginBottom: "10px",
            border: "2px solid #000",
            borderRadius: "10px",
            "&:last-of-type": {
                marginBottom: 0
            }
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__usersList__user}>
            <Typography>Имя пользователя: {props.name}</Typography>
            <Typography>Фамилия пользователя: {props.surname}</Typography>
            <Typography>Email пользователя: {props.email}</Typography>
        </Box>
    )
}

export default User;