import { MenuItem } from '@mui/material';
import { NavLink } from 'react-router-dom';
import { makeStyles } from "@mui/styles";

const LinkComponent = (props) => {
    const useStyles = makeStyles({
        app__header__menu__link: {
            color: "#fff",
            fontSize: "26px",
            fontWeight: 700
        },
        active: {
            color: "greenyellow"
        }
    });

    const classes = useStyles();

    return (
        <MenuItem>
            <NavLink to={props.link} className={({ isActive }) => isActive ? `${classes.app__header__menu__link} ${classes.active}` : classes.app__header__menu__link}>{props.name}</NavLink>
        </MenuItem>
    )
}

export default LinkComponent;