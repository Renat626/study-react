import LinkComponent from "./link/Link";
import { Box, MenuList } from '@mui/material';
import { makeStyles } from "@mui/styles";

const Header = () => {
    const useStyles = makeStyles({
        app__header: {
            paddingTop: "15px",
            paddingBottom: "15px",
            backgroundColor: "darkgray"
        },
        app__header__menu: {
            width: "100%",
            maxWidth: "1570px",
            margin: "0 auto",
            display: "flex",
            justifyContent: "space-between"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__header}>
            <MenuList className={classes.app__header__menu} sx={{margin: "0 auto"}}>
                <LinkComponent link="/" name="Главная" />
                <LinkComponent link="/shop" name="Магазин" />
                <LinkComponent link="/comments" name="Комментарии" />
                <LinkComponent link="/users-list" name="Список пользователей" />
                <LinkComponent link="/currencies" name="Курс валют" />
            </MenuList>
        </Box>
    )
}

export default Header;