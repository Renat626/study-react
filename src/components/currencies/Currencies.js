import { Box, Typography, Button, FormControl, Input, FormHelperText } from '@mui/material';
import { makeStyles } from "@mui/styles";
import { useDispatch, useSelector } from "react-redux";
import { getCurrency } from "../../store/actionCreators/currenciesActionCreators";
import { Line } from "react-chartjs-2";
import { Chart, registerables } from 'chart.js';
import { useRef } from "react";
import { Formik } from "formik";

Chart.register(...registerables)

const Currencies = () => {
    const date1 = useRef();
    const date2 = useRef();

    const useStyles = makeStyles({
        app__currencies: {
            padding: "10px",
            backgroundColor: "#dddddd"
        }
    })

    const classes = useStyles();

    const dispatch = useDispatch();

    const getCurrencyByClick = async () => {
        let re = /-/gi;
        console.log(date1.current.value.replace(re, "/").split("/").reverse().join("/"));
        await dispatch(getCurrency({date1: date1.current.value.replace(re, "/").split("/").reverse().join("/"), date2: date2.current.value.replace(re, "/").split("/").reverse().join("/"), currency: "R01235"}))
    }

    const currency = useSelector((state) => state.currencies.data);
    console.log(currency);

    let lineChartData = "";

    if (currency.length > 0) {
        let labels = [];
        let data = [];

        for (let i = 0; i < currency.length; i++) {
            labels.push(currency[i].date);
            data.push(currency[i].value);
        }

        console.log(data);

        lineChartData = {
            labels: labels,
            datasets: [
              {
                data: data,
                label: "rate",
                borderColor: "#3333ff",
                fill: true,
                lineTension: 0.5
              }
            ]
        };
    }

    return (
        <Box className={classes.app__currencies}>
            <Typography variant="h1">Currencies page</Typography>

            <Formik
                initialValues={{date1: "", date2: ""}}
                validate={values => {
                    const errors = {};

                    if (!values.date1) {
                        errors.date1 = "Вы не указали дату, с которой должен начаться курс валюты.";
                    }

                    if (!values.date2) {
                        errors.date2 = "Вы не указали дату, с которой должен начаться курс валюты.";
                    }

                    return errors;
                }}
                onSubmit={getCurrencyByClick}
            >
                {({
                    values,
                    errors,
                    handleChange,
                    handleBlur,
                    handleSubmit
                }) => (
                    <form onSubmit={handleSubmit}>
                        <FormControl>
                            <Input
                                type='date'
                                aria-describedby="date1"
                                value={values.date1} 
                                name="date1"
                                onChange={handleChange}
                                inputRef={date1}
                            />
                            <FormHelperText id="date1">От какого числа</FormHelperText>
                        </FormControl>

                        <FormControl>
                            <Input 
                                type='date'
                                aria-describedby="date2"
                                value={values.date2} 
                                name="date2"
                                onChange={handleChange}
                                inputRef={date2}
                            />
                            <FormHelperText id="date2">До какого числа</FormHelperText>
                        </FormControl>

                        <Button type='submit'>Получить курс валют</Button>
                    </form>
                )}
            </Formik>

            {/* {currency.length > 0 && (
                currency.map((item) => (
                    <Typography>{item.value}</Typography>
                ))
            )} */}

            {currency.length > 0 && (
                <Line
                    type="line"
                    width={160}
                    height={60}
                    options={{
                        title: {
                        display: true,
                        text: "COVID-19 Cases of Last 6 Months",
                        fontSize: 20
                        },
                        legend: {
                        display: true, //Is the legend shown?
                        position: "top" //Position of the legend.
                        }
                    }}
                    data={lineChartData}
                />
            )}  

            {/* <Line
                type="line"
                width={160}
                height={60}
                options={{
                    title: {
                    display: true,
                    text: "COVID-19 Cases of Last 6 Months",
                    fontSize: 20
                    },
                    legend: {
                    display: true, //Is the legend shown?
                    position: "top" //Position of the legend.
                    }
                }}
                data={lineChartData}
            /> */}
        </Box>
    )
}

export default Currencies;