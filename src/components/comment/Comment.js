import { useRef } from "react";
import { Box, Typography, FormControl, Input, Button } from '@mui/material';
import { create } from "../../store/actionCreators/commentActionCreators";
import { useDispatch, useSelector } from "react-redux";
import { Formik } from "formik";
import { makeStyles } from '@mui/styles';

const Comment = () => {
    let linkForInputOfComments = useRef();
    const dispatch = useDispatch();
    const accessToken = useSelector((state) => state.user.accessToken);

    const sendComment = () => {
        dispatch(create([{text: linkForInputOfComments.current.value}, {accessToken: accessToken}]));
    }

    const useStyles = makeStyles({
        app__comment: {
            padding: "10px",
            backgroundColor: "greenyellow"
        },

        app__comment__headline: {
            marginBottom: "50px"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__comment}>
            <Box className={classes.app__comment__headline}>
                <Typography variant="h1">Comment page</Typography>
            </Box>
            
            <Box>
                <Formik
                    initialValues={{comment: ""}}
                    validate={values => {
                        const errors = {};
                        if (!values.comment) {
                            errors.comment = "Обязательное поле!";
                        }

                        return errors;
                    }}
                    onSubmit={sendComment}
                    >
                    {({
                        values,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <FormControl>
                                <Input 
                                    placeholder="Введите комментарий"
                                    name="comment"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.comment} inputRef={linkForInputOfComments} />

                                    {errors.comment}

                                <Button type="submit">Отправить</Button>
                            </FormControl>
                        </form>
                    )}
                </Formik>
            </Box>
        </Box>
    )
}

export default Comment;