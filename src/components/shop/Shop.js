import { Box, Typography } from '@mui/material';
import { makeStyles } from "@mui/styles";

const Shop = () => {
    const useStyles = makeStyles({
        app__shop: {
            padding: "10px",
            backgroundColor: "aqua"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__shop}>
            <Typography variant="h1">Shop page</Typography>
        </Box>
    )
}

export default Shop;