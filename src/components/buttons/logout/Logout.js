import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../../store/actionCreators/userActionCreators";
import { Button } from '@mui/material';

const Logout = () => {
    const dispatch = useDispatch();
    const accessToken = useSelector((state) => state.user.accessToken);
    const logoutUser = () => {
        dispatch(logout(accessToken));
    }

    return (
        <Button onClick={logoutUser}>Выйти</Button>
    )
}

export default Logout;