import { Box, Typography } from '@mui/material';
import { makeStyles } from "@mui/styles";

const Main = (props) => {
    const useStyles = makeStyles({
        app__main: {
            padding: "10px",
            backgroundColor: "purple"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__main}>
            <Typography variant="h1">Main page</Typography>
        </Box>
    )
}

export default Main;