import { useRef } from "react";
import { useDispatch } from "react-redux";
import { registration } from "../../../store/actionCreators/userActionCreators";
import { makeStyles } from "@mui/styles";
import { Box, Typography, FormControl, Input, Button } from "@mui/material";
import { Formik } from "formik";

const Registration = () => {
    // const [name, setName] = useState("Renat");
    // const [surname, setSurname] = useState("Kasymov");
    // const [email, setEmail] = useState("kasymov.2002@mail.ru");
    // const [password, setPassword] = useState("123");
    const linkForInputOfEmail = useRef();
    const linkForInputOfPassword = useRef();
    const linkForInputOfName = useRef();
    const linkForInputOfSurname = useRef();
    const dispatch = useDispatch();

    const sendData = async () => {
        if (linkForInputOfName.current.value !== "" && linkForInputOfSurname.current.value !== "" && linkForInputOfEmail.current.value !== "" && linkForInputOfPassword.current.value !== "") {
            await dispatch(registration({name: linkForInputOfName.current.value, surname: linkForInputOfSurname.current.value, email: linkForInputOfEmail.current.value, password: linkForInputOfPassword.current.value}));
        }
    }

    const useStyles = makeStyles({
        app__containerForRegistration: {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
        },
        app__containerForRegistration__form: {
            padding: "20px",
            border: "2px solid #000",
            borderRadius: "10px",
            textAlign: "center",
            "& h1": {
                marginBottom: "10px"
            },
            "& input[type='text']": {
                border: "2px solid #000",
                marginBottom: "10px"
            }
        },
        app__containerForRegistration__form__container: {
            display: "flex",
            flexDirection: "column"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__containerForRegistration}>
            <Formik
                initialValues={{name: "", surname: "", email: "", password: ""}}
                validate={values => {
                    const errors = {}
                    if (!values.name) {
                        errors.name = "Это обязательное поле";
                    }

                    if (!values.surname) {
                        errors.surname = "Это обязательное поле";
                    }

                    if (!values.email) {
                        errors.email = "Это обязательное поле";
                    }

                    if (!values.password) {
                        errors.password = "Это обязательное поле";
                    }

                    return errors;
                }}
                onSubmit={sendData}
            >
                {({
                    values,
                    errors,
                    handleChange,
                    handleBlur,
                    handleSubmit
                }) => (
                    <form onSubmit={handleSubmit} className={classes.app__containerForRegistration__form}>
                        <Typography variant="h1">Регистрация</Typography>

                        <Box className={classes.app__containerForRegistration__form__container}>
                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="name" 
                                    placeholder="Имя пользователя" 
                                    value={values.name} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    inputRef={linkForInputOfName} />

                                {errors.name}
                            </FormControl>

                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="surname" 
                                    placeholder="Фамилия пользователя" 
                                    value={values.surname} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    inputRef={linkForInputOfSurname} />

                                {errors.surname}
                            </FormControl>

                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="email" 
                                    placeholder="Email пользователя" 
                                    value={values.email} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    inputRef={linkForInputOfEmail} />

                                {errors.email}
                            </FormControl>

                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="password" 
                                    placeholder="Пароль пользователя" 
                                    value={values.password} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    inputRef={linkForInputOfPassword} />

                                {errors.password}
                            </FormControl>

                            <Button type="submit">Регистрация</Button>
                        </Box>
                    </form>
                )}
            </Formik>
        </Box>
    )
}

export default Registration;