import { useRef } from "react";
import { useDispatch } from "react-redux";
import { login } from "../../../store/actionCreators/userActionCreators";
import { Box, Typography, FormControl, Input, Button } from "@mui/material";
import { Formik } from "formik";
import { makeStyles } from "@mui/styles";

const Login = () => {
    // const [email, setEmail] = useState("kasymov.2002@mail.ru");
    // const [password, setPassword] = useState("123");
    const linkForInputOfEmail = useRef();
    const linkForInputOfPassword = useRef();
    const dispatch = useDispatch();

    // const changeEmail = (e) => {
    //     console.log(e);
    //     setEmail(e.target.value);
    // }

    // const changePassword = (e) => {
    //     setPassword(e.target.value);
    // }

    const sendData = async () => {
        if (linkForInputOfEmail.current.value !== "" && linkForInputOfPassword.current.value !== "") {
            console.log(123);
            await dispatch(login({email: linkForInputOfEmail.current.value, password: linkForInputOfPassword.current.value}));
        }
    }

    const useStyles = makeStyles({
        app__containerForLogin: {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
        },
        app__containerForLogin__form: {
            padding: "20px",
            border: "2px solid #000",
            borderRadius: "10px",
            textAlign: "center",
            "& h1": {
                marginBottom: "10px"
            },
            "& input[type='text']": {
                border: "2px solid #000",
                marginBottom: "10px"
            }
        },
        app__containerForLogin__form__container: {
            display: "flex",
            flexDirection: "column"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__containerForLogin}>
            <Formik
                initialValues={{email: "", password: ""}}
                validate={values => {
                    const errors = {};
                    if (!values.email) {
                        errors.email = "Это поле обязательное";
                    }

                    if (!values.password) {
                        errors.password = "Это поле обязательное";
                    }

                    return errors;
                }}
                onSubmit={sendData}
            >  
                {({
                    values,
                    errors,
                    handleChange,
                    handleBlur,
                    handleSubmit
                }) => (
                    <form onSubmit={handleSubmit} className={classes.app__containerForLogin__form}>
                        <Typography variant="h1">Авторизация</Typography>

                        <Box className={classes.app__containerForLogin__form__container}>
                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="email" 
                                    placeholder="Email пользователя" 
                                    value={values.email} 
                                    onChange={handleChange}
                                    onBlur={handleBlur} 
                                    inputRef={linkForInputOfEmail} />

                                {errors.email}
                            </FormControl>

                            <FormControl>
                                <Input 
                                    type="text" 
                                    name="password" 
                                    placeholder="Пароль пользователя" 
                                    value={values.password} 
                                    onChange={handleChange}
                                    onBlur={handleBlur} 
                                    inputRef={linkForInputOfPassword} />

                                {errors.password}
                            </FormControl>

                            <Button type="submit">Авторизация</Button>
                        </Box>
                    </form>
                )}
            </Formik>
        </Box>
    )
}

export default Login;