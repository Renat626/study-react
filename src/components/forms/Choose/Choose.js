import { Box, MenuList, MenuItem } from '@mui/material';
import { NavLink } from 'react-router-dom';
import { makeStyles } from "@mui/styles";

const Choose = () => {
    const useStyles = makeStyles({
        app__containerForChoose: {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
        }
    })

    const classes = useStyles();

    return (
        <Box className={classes.app__containerForChoose}>
            <MenuList>
                <MenuItem>
                    <NavLink to="/registration">Регистрация</NavLink>
                </MenuItem>
                <MenuItem>
                    <NavLink to="/login">Авторизация</NavLink>
                </MenuItem>
            </MenuList>
        </Box>
    )
}

export default Choose;