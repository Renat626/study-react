import './App.css';
import { useCallback, useEffect } from 'react';
import { check } from "./store/actionCreators/userActionCreators";
import Header from "./components/header/Header";
import Main from "./components/main/Main";
import Shop from "./components/shop/Shop";
import Comment from "./components/comment/Comment";
import UsersList from './components/users_list/UsersList';
import Registration from './components/forms/Registration/Registration';
import Login from './components/forms/Login/Login';
import Choose from './components/forms/Choose/Choose';
import Logout from "./components/buttons/logout/Logout";
import Currencies from './components/currencies/Currencies';
import { Box } from '@mui/material';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

function App() {
  const isLogin = useSelector((state) => state.user.isLogin);
  const accessToken = useSelector((state) => state.user.accessToken);
  const dispatch = useDispatch();

  const checkLogin = useCallback(async () => {
    await dispatch(check(accessToken));
  }, [dispatch, accessToken])

  useEffect(() => {
    checkLogin();
  }, [dispatch, checkLogin])

  console.log({isLogin, accessToken});

  // console.log(`token: ${localStorage.getItem("access_token")}`);
  // console.log(`isLogin: ${localStorage.getItem("isLogin")}`);

  return (
    <Box className="app">
      {isLogin !== true && (
        <Router>
          <Routes>
            <Route path="/" element={<Choose />} />
            <Route path="/registration" element={<Registration />} />
            <Route path="/login" element={<Login />} />
            <Route path="/shop" element={<Navigate to="/" />} />
            <Route path="/comments" element={<Navigate to="/" />} />
            <Route path="/users-list" element={<Navigate to="/" />} />
            <Route path="/currencies" element={<Navigate to="/" />} />
          </Routes>
        </Router>
        )}

      {isLogin === true && (
        <Router>
          <Header />
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/comments" element={<Comment />} />
            <Route path="/users-list" element={<UsersList />} />
            <Route path="/currencies" element={<Currencies />} />
            <Route path="/login" element={<Navigate to="/" />} />
            <Route path="/registration" element={<Navigate to="/" />} />
          </Routes>
          <Logout />
        </Router>
        )}
    </Box>
  );
}

export default App;
