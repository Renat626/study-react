import axios from "axios";
import { url } from "../url/baseUrls";

const axiosCreate = axios.create({
    timeout: 2000
});

export const axiosUser = {
    registration: async (data) => {
        try {
            const answer = await axiosCreate.post(`${url}api/user/registration`, data);
            localStorage.setItem("access_token", answer.data.access_token);
            localStorage.setItem("isLogin", true);
            return {accessToken: answer.data.access_token, isLogin: true};
        } catch (error) {
            console.log(error);
            return error;
        }
    },

    login: async (data) => {
        try {
            const answer = await axiosCreate.post(`${url}api/user/login`, data);
            localStorage.setItem("access_token", answer.data.access_token);
            localStorage.setItem("isLogin", true);
            return {accessToken: answer.data.access_token, isLogin: true};
        } catch (error) {
            console.log(error);
            return error;
        }
    },

    check: async (data) => {
        try {
            const answer = await axiosCreate.get(`${url}api/user/check`, {
                headers: {
                    Authorization: `Bearer ${data}`
                }
            });
            localStorage.setItem("isLogin", answer.data);
            return answer.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    },

    logout: async (data) => {
        try {
            const answer = await axiosCreate.get(`${url}api/user/logout`, {
                headers: {
                    Authorization: `Bearer ${data}`
                }
            });
            localStorage.setItem("access_token", "");
            localStorage.setItem("isLogin", false);
            return {accessToken: "", isLogin: false};
        } catch (error) {
            console.log(error);
            return error;
        }
    },

    getAll: async (data) => {
        try {
            const answer = await axiosCreate.get(`${url}api/user/get-all`, {
                headers: {
                    Authorization: `Bearer ${data}`
                }
            });
            return answer.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    } 
}