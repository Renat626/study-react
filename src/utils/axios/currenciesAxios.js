import axios from "axios";
import { url } from "../url/baseUrls";

const axiosCreate = axios.create({
    timeout: 2000
});

export const currenciesAxios = {
    getCurrency: async (data) => {
        try {
            const answer = await axiosCreate.post(`${url}api/currencies/get-currency`, data);
            console.log(answer.data);
            return answer.data
        } catch (error) {
            console.log(error);
        }
    }
}