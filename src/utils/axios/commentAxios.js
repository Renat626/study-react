import axios from "axios";
import { url } from "../url/baseUrls";

const axiosCreate = axios.create({
    timeout: 2000
});

export const commentAxios = {
    create: async (data) => {
        try {
            const answer = await axiosCreate.post(`${url}api/comment/create`, data[0], {
                headers: {
                    Authorization: `Bearer ${data[1].accessToken}`
                }
            });
            return {text: data};
        } catch (error) {
            console.log(error);
            return error;
        }
    },

    getAll: async (data) => {
        try {
            const answer = await axiosCreate.get(`${url}api/comment/get-all`, {
                headers: {
                    Authorization: `Bearer ${data}`
                }
            });
            return answer.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    } 
}