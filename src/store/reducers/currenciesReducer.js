import { currenciesState } from "../states/currenciesState";
import { currenciesActions } from "../actions/currenciesActions";

const currenciesReducer = (state = currenciesState, action) => {
    switch (action.type) {
        case currenciesActions.GET_CURRENCY:
            return {...state, data: action.payload};
        default:
            return state;
    }
}

export default currenciesReducer;