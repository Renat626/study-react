import commentState from "../states/commentState";
import { commentActions } from "../actions/commentActions";

const commentReducer = (state = commentState, action) => {
    switch (action.type) {
        case commentActions.CREATE:
            return {...state, text: action.payload.text};
        case commentActions.GET_ALL:
            return {...state, users_list: action.payload};
        default:
            return state;
    }
}

export default commentReducer;