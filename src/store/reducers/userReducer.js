import userState from "../states/userState";
import { userActions } from "../actions/userActions";

const userReducer = (state = userState, action) => {
    switch (action.type) {
        case userActions.REGISTRATION:
            return {...state, accessToken: action.payload.accessToken, isLogin: action.payload.isLogin};
        case userActions.LOGIN:
            return {...state, accessToken: action.payload.accessToken, isLogin: action.payload.isLogin};
        case userActions.CHECK:
            return {...state, isLogin: action.payload};
        case userActions.LOGOUT:
            return {...state, accessToken: action.payload.accessToken, isLogin: action.payload.isLogin};
        case userActions.GET_ALL:
            return {...state, users_list: action.payload};
        default:
            return state;
    }
}

export default userReducer;