import { createStore, applyMiddleware, combineReducers } from "redux";
import userReducer from "./reducers/userReducer";
import commentReducer from "./reducers/commentReducer";
import currenciesReducer from "./reducers/currenciesReducer";
import thunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducers = combineReducers({
    user: userReducer,
    comment: commentReducer,
    currencies: currenciesReducer
})
const store = createStore(rootReducers, composeWithDevTools(applyMiddleware(thunk)));

export default store;