const userState = {
    id: 0,
    accessToken: !localStorage.getItem("access_token") ? "" : localStorage.getItem("access_token"),
    isLogin: !localStorage.getItem("isLogin") ? false : JSON.parse(localStorage.getItem("isLogin")),
    users_list: []
}

export default userState;