import { axiosUser } from "../../utils/axios/userAxios";
import { userActions } from "../actions/userActions";

export const registration = (data) => async (dispatch) => {
    try {
        const answer = await axiosUser.registration(data);
        await dispatch(setRegistration(answer));
    } catch (error) {
        console.log(error);
    }
}

export const login = (data) => async (dispatch) => {
    try {
        const answer = await axiosUser.login(data);
        await dispatch(setLogin(answer));
    } catch (error) {
        console.log(error);
    }
}

export const check = (data) => async (dispatch) => {
    try {
        const answer = await axiosUser.check(data);
        await dispatch(setCheck(answer));
    } catch (error) {
        console.log(error);
    }
}

export const logout = (data) => async (dispatch) => {
    try {
        const answer = await axiosUser.logout(data);
        await dispatch(setLogout(answer));
    } catch (error) {
        console.log(error);
    }
}

export const getAll = (data) => async (dispatch) => {
    try {
        const answer = await axiosUser.getAll(data);
        await dispatch(setGetAll(answer));
    } catch (error) {
        console.log(error);
    }
}

export const setRegistration = (payload) => (
    {
        type: userActions.REGISTRATION,
        payload
    }
)

export const setLogin = (payload) => (
    {
        type: userActions.LOGIN,
        payload
    }
)


export const setCheck = (payload) => (
    {
        type: userActions.CHECK,
        payload
    }
)

export const setLogout = (payload) => (
    {
        type: userActions.LOGOUT,
        payload
    }
)

export const setGetAll = (payload) => (
    {
        type: userActions.GET_ALL,
        payload
    }
)