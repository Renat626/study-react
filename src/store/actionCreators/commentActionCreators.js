import { commentAxios } from "../../utils/axios/commentAxios";
import { commentActions } from "../actions/commentActions";

export const create = (data) => async (dispatch) => {
    try {
        const answer = await commentAxios.create(data);
        await dispatch(setCreate(answer));
    } catch (error) {
        console.log(error);
    }
}

export const getAll = (data) => async (dispatch) => {
    try {
        const answer = await commentAxios.getAll(data);
        await dispatch(setGetAll(answer));
    } catch (error) {
        console.log(error);
    }
}

export const setCreate = (payload) => (
    {
        type: commentActions.CREATE,
        payload
    }
)

export const setGetAll = (payload) => (
    {
        type: commentActions.GET_ALL,
        payload
    }
)