import { currenciesAxios } from "../../utils/axios/currenciesAxios";
import { currenciesActions } from "../actions/currenciesActions";

export const getCurrency = (data) => async (dispatch) => {
    try {
        const answer = await currenciesAxios.getCurrency(data);
        await dispatch(setCurrency(answer));
    } catch (error) {
        console.log(error);
    }
}

export const setCurrency = (payload) => (
    {
        type: currenciesActions.GET_CURRENCY,
        payload
    }
)